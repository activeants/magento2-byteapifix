<?php
namespace ActiveAnts\ByteApiFix\Plugin\Framework\Webapi;

use Magento\Framework\Webapi\Request;

/**
 * Class RequestPlugin
 * @package ActiveAnts\ByteApiFix\Plugin\Framework\Webapi
 */
class RequestPlugin
{
    /**
     * @param Request $subject
     * @param callable $proceed
     * @param $header
     * @return mixed
     */
    public function aroundGetHeader(Request $subject, callable $proceed, $header){
        $headerValue = $proceed($header);
        if ($headerValue == false) {
            /** Workaround for Byte environment */
            $header = 'REDIRECT_BYTE_' . strtoupper(str_replace('-', '_', $header));
            if (isset($_SERVER[$header])) {
                $headerValue = $_SERVER[$header];
            }
        }
        return $headerValue;
    }
}