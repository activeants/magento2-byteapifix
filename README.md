# [![Active Ants](http://activeants.nl/content/themes/active-ants/img/logo.png)](http://activeants.nl/) 

## Magento 2 Byte Api Fix
An extension to fix the use of the Magento2 API's under Byte hosting (https://www.byte.nl/kennisbank/item/php_cgi_modus).

## Motivation
On of our webshops is hosted at Byte (https://byte.nl). Byte uses PHP under CGI mode. 
Authentication (Basic, Digest etc.) will not work out of the box and we wanted that to work without hacks. 

## Technical feature
This extension uses a plugin (http://devdocs.magento.com/guides/v2.0/extension-dev-guide/plugins.html) 
to access the REDIRECT_* variables from the environment variables as header variables.
 
## Installation
```
composer require activeants/magento2-byteapifix
php bin/magento module:enable ActiveAnts_ByteApiFix
php bin/magento setup:upgrade
```

Modify the following rule in the .htaccess file:

```
RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
```

to

```
RewriteRule .* - [E=BYTE_AUTHORIZATION:%{HTTP:Authorization}]
```

## Issues
Feel free to report any issues for this project using the integrated [Bitbucket issue tracker](https://bitbucket.org/activeants/magento2-byteapifix/issues), we will try to get back to issues as fast as possible.

### License
The Byte Api Fix is free software, and is released under the terms of the OSL 3.0 license. See LICENSE.md for more information.

## Credits
[Active Ants](http://activeants.nl/) provides e-commerce efulfilment services for webshops. We store products, pick, pack and ship them. But we do much more. With unique efulfilment solutions we make our clients, the webshops, more successful.

~ The [Active Ants](http://activeants.nl/) software development team.